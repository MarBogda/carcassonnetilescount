import sqlite3
import json


class Database:
    def __init__(self, filename='../data/games.sql'):
        self.filename = filename
        # only one connection - don't use with multiple threads!
        self.conn = sqlite3.connect(self.filename,
                                    detect_types=sqlite3.PARSE_DECLTYPES)
        self.cur = self.conn.cursor()
        self.cur.execute('PRAGMA foreign_keys = ON;')
        if not self.cur.execute(
                "SELECT name FROM sqlite_master WHERE type = 'table'"
        ).fetchall():
            self.create()

    def close(self):
        self.conn.close()

    def create(self):
        with open('db_init.sql', 'r') as f:
            self.cur.executescript(f.read())
            with open('../res/carc0/carc0.json') as carc0:
                carc0_json = json.load(carc0)
                self.cur.execute('''\
INSERT INTO carc0 (tile_0, tile_1, tile_2, tile_3, tile_4, tile_5, tile_6,
  tile_7, tile_8, tile_9, tile_10, tile_11, tile_12, tile_13, tile_14, tile_15,
  tile_16, tile_17, tile_18, tile_19, tile_20, tile_21, tile_22, tile_23)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
''',
                                 carc0_json)
            with open('../res/carc1/carc1.json') as carc1:
                carc1_json = json.load(carc1)
                self.cur.execute('''\
INSERT INTO carc1 (tile_0, tile_1, tile_2, tile_3, tile_4, tile_5, tile_6,
  tile_7, tile_8, tile_9, tile_10, tile_11, tile_12, tile_13, tile_14, tile_15,
  tile_16)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
''',
                                 carc1_json)
            with open('../res/carc2/carc2.json') as carc2:
                carc2_json = json.load(carc2)
                self.cur.execute('''\
INSERT INTO carc2 (tile_0, tile_1, tile_2, tile_3, tile_4, tile_5, tile_6,
  tile_7, tile_8, tile_9, tile_10, tile_11, tile_12, tile_13, tile_14, tile_15,
  tile_16, tile_17, tile_18, tile_19, tile_20, tile_21, tile_22, tile_23)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
''',
                                 carc2_json)
        self.conn.commit()

    def select_games(self):
        return self.cur.execute(
            'SELECT rowid, name FROM Games WHERE deleted = 0').fetchall()

    def select_game(self, rowid):
        return self.cur.execute('''\
SELECT g.rowid, g.name, c1.*, c2.*, c3.*
FROM Games AS g
  LEFT OUTER JOIN carc0 AS c1 ON g.carc0_id = c1.rowid
  LEFT OUTER JOIN carc1 c2 ON g.carc1_id = c2.rowid
  LEFT OUTER JOIN carc2 c3 ON g.carc2_id = c3.rowid
WHERE g.rowid = ? AND g.deleted = 0
''',
                                (rowid,)).fetchone()

    def insert_game(self, name, carc0=False, carc1=False, carc2=False):
        if carc0:
            self.cur.execute('''\
INSERT INTO carc0 (tile_0, tile_1, tile_2, tile_3, tile_4, tile_5, tile_6,
  tile_7, tile_8, tile_9, tile_10, tile_11, tile_12, tile_13, tile_14, tile_15, 
  tile_16, tile_17, tile_18, tile_19, tile_20, tile_21, tile_22, tile_23)
SELECT tile_0, tile_1, tile_2, tile_3, tile_4, tile_5, tile_6, tile_7, 
    tile_8, tile_9, tile_10, tile_11, tile_12, tile_13, tile_14, tile_15, 
    tile_16, tile_17, tile_18, tile_19, tile_20, tile_21, tile_22, tile_23
  FROM carc0 WHERE rowid = 1
''')
            carc0 = self.cur.lastrowid
        else:
            carc0 = None

        if carc1:
            self.cur.execute('''\
INSERT INTO carc1 (tile_0, tile_1, tile_2, tile_3, tile_4, tile_5, tile_6,
  tile_7, tile_8, tile_9, tile_10, tile_11, tile_12, tile_13, tile_14, tile_15, 
  tile_16)
SELECT tile_0, tile_1, tile_2, tile_3, tile_4, tile_5, tile_6, tile_7, 
    tile_8, tile_9, tile_10, tile_11, tile_12, tile_13, tile_14, tile_15, 
    tile_16
  FROM carc1 WHERE rowid = 1
''')
            carc1 = self.cur.lastrowid
        else:
            carc1 = None

        if carc2:
            self.cur.execute('''\
INSERT INTO carc2 (tile_0, tile_1, tile_2, tile_3, tile_4, tile_5, tile_6,
  tile_7, tile_8, tile_9, tile_10, tile_11, tile_12, tile_13, tile_14, tile_15, 
  tile_16, tile_17, tile_18, tile_19, tile_20, tile_21, tile_22, tile_23)
SELECT tile_0, tile_1, tile_2, tile_3, tile_4, tile_5, tile_6, tile_7, 
    tile_8, tile_9, tile_10, tile_11, tile_12, tile_13, tile_14, tile_15, 
    tile_16, tile_17, tile_18, tile_19, tile_20, tile_21, tile_22, tile_23
  FROM carc2 WHERE rowid = 1
''')
            carc2 = self.cur.lastrowid
        else:
            carc2 = None

        self.cur.execute(
            'INSERT INTO Games (name, deleted, carc0_id, carc1_id, carc2_id) '
            'VALUES (?, 0, ?, ?, ?)',
            (name, carc0, carc1, carc2),
        )
        self.conn.commit()
        return self.cur.lastrowid

    def update_game(self, game_id, name, carc0=None, carc1=None, carc2=None):
        # update game name
        self.cur.execute(
            'UPDATE Games SET name = ? WHERE game_id = ? AND deleted = 0',
            (name, game_id),
        )

        # update game values
        carc0_id, carc1_id, carc2_id = self.cur.execute(
            'SELECT carc0_id, carc1_id, carc2_id FROM Games WHERE game_id = ? '
            'AND deleted = 0',
            (game_id,),
        ).fetchone()
        if carc0:
            self.cur.execute('''\
UPDATE carc0
SET tile_0 = ?, tile_1 = ?, tile_2 = ?, tile_3 = ?, tile_4 = ?, tile_5 = ?,
  tile_6 = ?, tile_7 = ?, tile_8 = ?, tile_9 = ?, tile_10 = ?, tile_11 = ?,
  tile_12 = ?, tile_13 = ?, tile_14 = ?, tile_15 = ?, tile_16 = ?, tile_17 = ?,
  tile_18 = ?, tile_19 = ?, tile_20 = ?, tile_21 = ?, tile_22 = ?, tile_23 = ?
WHERE carc0_id = ?
''',
                             carc0 + [carc0_id])
        if carc1:
            self.cur.execute('''\
UPDATE carc1
SET tile_0 = ?, tile_1 = ?, tile_2 = ?, tile_3 = ?, tile_4 = ?, tile_5 = ?,
  tile_6 = ?, tile_7 = ?, tile_8 = ?, tile_9 = ?, tile_10 = ?, tile_11 = ?,
  tile_12 = ?, tile_13 = ?, tile_14 = ?, tile_15 = ?, tile_16 = ?
WHERE carc1_id = ?
''',
                             carc1 + [carc1_id])
        if carc2:
            self.cur.execute('''\
UPDATE carc2
SET tile_0 = ?, tile_1 = ?, tile_2 = ?, tile_3 = ?, tile_4 = ?, tile_5 = ?,
  tile_6 = ?, tile_7 = ?, tile_8 = ?, tile_9 = ?, tile_10 = ?, tile_11 = ?,
  tile_12 = ?, tile_13 = ?, tile_14 = ?, tile_15 = ?, tile_16 = ?, tile_17 = ?,
  tile_18 = ?, tile_19 = ?, tile_20 = ?, tile_21 = ?, tile_22 = ?, tile_23 = ?
WHERE carc2_id = ?
''',
                             carc2 + [carc2_id])
        self.conn.commit()

    def delete_game(self, game_id):
        self.cur.execute(
            'UPDATE Games SET deleted = 1 WHERE game_id = ?',
            (game_id,),
        )
        self.conn.commit()
