-- todo: constraint on amount

CREATE TABLE IF NOT EXISTS carc0 (
  carc0_id INTEGER PRIMARY KEY,
  tile_0 INTEGER,
  tile_1 INTEGER,
  tile_2 INTEGER,
  tile_3 INTEGER,
  tile_4 INTEGER,
  tile_5 INTEGER,
  tile_6 INTEGER,
  tile_7 INTEGER,
  tile_8 INTEGER,
  tile_9 INTEGER,
  tile_10 INTEGER,
  tile_11 INTEGER,
  tile_12 INTEGER,
  tile_13 INTEGER,
  tile_14 INTEGER,
  tile_15 INTEGER,
  tile_16 INTEGER,
  tile_17 INTEGER,
  tile_18 INTEGER,
  tile_19 INTEGER,
  tile_20 INTEGER,
  tile_21 INTEGER,
  tile_22 INTEGER,
  tile_23 INTEGER
);

CREATE TABLE IF NOT EXISTS carc1 (
  carc1_id INTEGER PRIMARY KEY,
  tile_0 INTEGER,
  tile_1 INTEGER,
  tile_2 INTEGER,
  tile_3 INTEGER,
  tile_4 INTEGER,
  tile_5 INTEGER,
  tile_6 INTEGER,
  tile_7 INTEGER,
  tile_8 INTEGER,
  tile_9 INTEGER,
  tile_10 INTEGER,
  tile_11 INTEGER,
  tile_12 INTEGER,
  tile_13 INTEGER,
  tile_14 INTEGER,
  tile_15 INTEGER,
  tile_16 INTEGER
);

CREATE TABLE IF NOT EXISTS carc2 (
  carc2_id INTEGER PRIMARY KEY,
  tile_0 INTEGER,
  tile_1 INTEGER,
  tile_2 INTEGER,
  tile_3 INTEGER,
  tile_4 INTEGER,
  tile_5 INTEGER,
  tile_6 INTEGER,
  tile_7 INTEGER,
  tile_8 INTEGER,
  tile_9 INTEGER,
  tile_10 INTEGER,
  tile_11 INTEGER,
  tile_12 INTEGER,
  tile_13 INTEGER,
  tile_14 INTEGER,
  tile_15 INTEGER,
  tile_16 INTEGER,
  tile_17 INTEGER,
  tile_18 INTEGER,
  tile_19 INTEGER,
  tile_20 INTEGER,
  tile_21 INTEGER,
  tile_22 INTEGER,
  tile_23 INTEGER
);

/*CREATE TABLE IF NOT EXISTS carc3 (
  carc3_id INTEGER PRIMARY KEY,
  tile_0 INTEGER,
  tile_1 INTEGER,
  tile_2 INTEGER,
  tile_3 INTEGER,
  tile_4 INTEGER,
  tile_5 INTEGER,
  tile_6 INTEGER,
  tile_7 INTEGER,
  tile_8 INTEGER,
  tile_9 INTEGER,
  tile_10 INTEGER,
  tile_11 INTEGER
);

CREATE TABLE IF NOT EXISTS carc4 (
  carc4_id INTEGER PRIMARY KEY,
  tile_0 INTEGER,
  tile_1 INTEGER,
  tile_2 INTEGER,
  tile_3 INTEGER,
  tile_4 INTEGER
);*/

CREATE TABLE IF NOT EXISTS Games (
  game_id   INTEGER PRIMARY KEY,
  name      TEXT,
  deleted   INTEGER,
  carc0_id  INTEGER REFERENCES carc0  ( carc0_id ),
  carc1_id  INTEGER REFERENCES carc1  ( carc1_id ),
  carc2_id  INTEGER REFERENCES carc2  ( carc2_id )
--   carc3_id  INTEGER REFERENCES carc3  ( carc3_id ),
--   carc4_id  INTEGER REFERENCES carc4  ( carc4_id )
);
