import bottle

import database


@bottle.route('/', method='GET')
def index():
    return bottle.static_file('main.html', './')


@bottle.route('/favicon.ico', method='GET')
def favicon():
    return bottle.static_file('0.jpg', '../res/carc0')


@bottle.route('/res/<path:path>/<file>', method='GET')
def resource(path, file):
    return bottle.static_file(file, '../res/{}'.format(path))


@bottle.route('/games', method='GET')
def get_games():
    return {'games': [{'game_id': game_id, 'game_name': name}
            for game_id, name in db.select_games()]}


@bottle.route('/game/<game_id>', method='GET')
def get_game(game_id):
    values = db.select_game(game_id)
    return {
        # data without IDs of carcN tables
        'game_id': values[0],
        'game_name': values[1],
        'carc0': values[3:27],
        'carc1': values[28:45],
        'carc2': values[46:],
    }


@bottle.route('/game/<game_id>', method='PUT')
def update_game(game_id):
    game = bottle.request.json
    db.update_game(
        game_id, game['game_name'],
        game['carc0'], game['carc1'], game['carc2']
    )
    return get_game(game_id)


@bottle.route('/game/<game_id>', method='DELETE')
def delete_game(game_id):
    db.delete_game(game_id)


@bottle.route('/game/new', method='POST')
def new_game():
    game = bottle.request.json
    return get_game(db.insert_game(
            game['game_name'],
            game.get('carc0', False),
            game.get('carc1', False),
            game.get('carc2', False),
    ))


if __name__ == '__main__':
    db = database.Database()
    bottle.run(host='localhost', port=8080, debug=True)
    db.close()
